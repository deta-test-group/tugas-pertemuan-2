<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Berita extends BaseController
{
    public function xml_1()
    {
        $berita_xml_1 = file_get_contents('https://rss.tempo.co/bisnis');
        $json_1 = simplexml_load_string($berita_xml_1);
        $data['berita_xml_1'] = $json_1;
        return view('xml_1/index', $data);
    }

    public function xml_2()
    {
        $berita_xml_2 = simplexml_load_file('https://www.cnnindonesia.com/ekonomi/rss');
        // $image = $berita_xml_2->channel->item[0]->enclosure->attributes()->url;
        $data['berita_xml_2'] = $berita_xml_2;
        return view('xml_2/index', $data);
    }

    public function xml_3()
    {
        $berita_xml_3 = simplexml_load_file('https://www.republika.co.id/rss');
        $data['berita_xml_3'] = $berita_xml_3;
        return view('xml_3/index', $data);
    }

    public function json_portal()
    {
        $berita_json_portal = file_get_contents('https://data.bmkg.go.id/DataMKG/TEWS/gempadirasakan.json');
        $json_portal = json_decode($berita_json_portal, true);
        // get last 15 data
        $data['berita_json_portal'] = $json_portal;
        // $data['berita_json_portal'] = $json_portal;
        return view('json/index', $data);
    }
}
